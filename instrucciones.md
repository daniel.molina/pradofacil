@def title = "Formulario para crear cuestionarios Moodle"
@def hasmath = false
@def hascode = false
<!-- Note: by default hasmath == true and hascode == false. You can change this in
the config file by setting hasmath = false for instance and just setting it to true
where appropriate -->
\textinput{motivacion}
\textinput{menu.md}

\subtitle{Instrucciones de Formato}

La sintaxis es muy sencilla, y en modo texto, en donde los espacios no son
tenidos en cuenta. Se compone de los siguientes tipos de preguntas:

\subsection{Preguntas con Opciones (Respuesta única y múltiple)}

Estas preguntas se listan sin ningún tipo de símbolo inicial. Tras cada
pregunta se debe de indicar las distintas opciones posibles, precedidas cada una
de ellas con "-" ó "+". La opción con un "+" es la verdadera, el resto son
falsas. El orden de aparición de las opciones es aleatorio, excepto en el caso
de que una de las respuestas contenga _A_, _B_, _C_, ... para permitir respuestas
del tipo "Las respuestas A y C son correctas". En ese caso el orden no es aleatorio.

@@message
**Ejemplos:**

El coronavirus es el virus, y la enfermedad se llama:

~~~
- Sueño.<br>
+ Covid-19.<br>
- Resfriado.<br>
<br>
~~~

A la hora de salir debo:
~~~
<br>
+ Intentar mantener la distancia de seguridad.<br>
- Ir rápido y con ropa de deporte.<br>
+ Ir con mascarilla.<br>
~~~

A la hora de ir de compras debo:
~~~
<br>
- Respetar la distancia en la cola.<br>
- Entrar sin mirar.<br>
- Cumplir el límite de aforo.<br>
+ La opción A y C son correctas.<br>
<br>
~~~
@@

Aunque por defecto supone preguntas de una línea, también se pueden usar
preguntas usando varios párrafos usando el separador \`.

@@message
**Ejemplo:**

\`¿Quién dijo la siguiente frase?:
~~~
<br>
<br>
"No tengo un talento especial. Sólo soy apasionadamente curioso."<br>
&#96;
<br>
- Descartes.<br>
+ Albert Eistein.<br>
- Platon.<br>
~~~
@@

\subsection{Preguntas de tipo Verdadero/Falso}

Las preguntas verdadero o falso se describen con una frase terminada en "-" (si
la respuesta correcta es "Falso") o terminada en "+" (si la respuesta correcta
es "Verdadero"). 

@@message
**Ejemplos:**

El confinamiento es recomendable para evitar difundir el coronavirus. +

No es recomendable el uso de mascarillas. -
@@

\subsection{Preguntas de tipo Ensayo}

Las preguntas de tipo ensayo, en la que el alumno o la alumna debe de indicar un
texto que será valorado por el profesorado, se puede indicar con una frase
totalmente entre corchetes.

@@message
**Ejemplo:**

[Motive por qué el confinamiento reduce la expansión del virus.]
@@

\subsection{Preguntas Cortas}

Las preguntas cortas son en las que el alumno introduce directamente la
respuesta, y existe una solución correcta, que verifica el sistema, las
mayúsculas no son importantes. Es recomendable únicamente para respuestas muy
cortas (como una palabra). La sintaxis es la pregunta, y a continuación la
respuesta correcta entre corchetes.

@@message
**Ejemplo:**

El estar encerrado en casa para evitar el contagio se llama... [confinamiento]
@@

\subsection{Categorías}

Las categorías son agrupaciones de preguntas, se indican con un asterisco y su
nombre antes del conjunto de preguntas que se englobarán en dicha categoría. Son
opcionales. Por cada categoría se generará un fichero XML que se deberá de
importar en **Moodle** como *XML Moodle**.

@@message
**Ejemplo:**

~~~
* Categoría Salud<br>
<br>
Primera Pregunta de salud.<br>
- Opción 1.<br>
+ Opción 2.<br>
<br>
Segunda pregunta de salud.<br>
...<br>
<br>
* Categoría Medio Ambiente<br>

Primera pregunta de medio ambiente</br>
...<br>
~~~
@@
