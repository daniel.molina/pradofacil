
Con la llegada del coronavirus, el uso de PRADO/Moodle se ha convertido en una
herramienta imprescindible, especialmente para la realización de pruebas
evaluables o de autoevaluación mediante el uso de Cuestionarios.

Para facilitar la creación de este tipo de cuestionarios al profesorado en
general, esta página web permite generar, a partir de una sintaxis muy sencilla,
ficheros que pueden importarse en PRADO como Bancos de preguntas (como formato
XMLMoodle).

