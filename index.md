@def title = "Formulario para crear cuestionarios Moodle"
@def hasmath = false
@def hascode = false
<!-- Note: by default hasmath == true and hascode == false. You can change this in
the config file by setting hasmath = false for instance and just setting it to true
where appropriate -->
\textinput{motivacion}
\textinput{menu.md}

~~~
<div class="field">
<form action="http://{{fill server}}/send" method="POST" id="send" enctype="multipart/form-data">
<input type="hidden" name="language" value="es"/>
<h1 class="field subtitle is-4">Penalización por pregunta incorrecta</h1>
<div class="columns">
<div class="column is-two-quarters">
<div class="field is-horizontal">
<label class="field subtitle is-4">Pregunta opción múltiple &nbsp;&nbsp;</label>
  <div class="control">
      <div class="select">
      <select name="penalty_options" form="send">
        <option value="0" selected>0</option>
        <option value="0.25">0.25</option>
        <option value="0.33">0.33</option>
        <option value="0.5">0.5</option>
        <option value="0.75">0.75</option>
        <option value="1">1</option>
      </select>
    </div>
  </div>
</div>
</div>
<div class="column is-two-quarters">
<div class="field is-horizontal">
<label class="field subtitle is-4">&nbsp;&nbsp;&nbsp;Pregunta Verdadero/Falso&nbsp;&nbsp;</label>
  <div class="control">
    <div class="select">
      <select name="penalty_boolean">
        <option value="0" selected>0</option>
        <option value="0.25">0.25</option>
        <option value="0.33">0.33</option>
        <option value="0.5">0.5</option>
        <option value="0.75">0.75</option>
        <option value="1">1</option>
      </select>
    </div>
  </div>
  </div>
</div>
</div>
<div class="column is-4 is-offset-6">
<label class="checkbox" for="mul">Verdadero/Falso para múltiple</label>
<input type="checkbox" name="multiple" id="mul">
</div>
<h1 class="subtitle is-4">Ponga aquí su texto</h1>
<div class="control">
    <textarea name="text" class="textarea is-medium"
              placeholder="Ponga aquí la descripción" rows="10" form="send"></textarea>
</div>
</div>
<div class="field is-grouped">
    <div class="control">
        <button type="submit" class="button is-link is-medium">Convertir</button>
    </div>
    <div class="control">
        <button class="button is-link is-light is-medium" type="reset">Cancelar</button>
    </div>
</div>
</form>
 ~~~

# Importación del fichero

Tras el formulario se puede recibir un único fichero .XML o un zip con los
distintos ficheros .XML, uno por cada categoría, que se deben de importir uno a
uno en nuestro banco de preguntas, según el siguiente proceso:

1. Ir al curso Moodle en donde lo queramos importar.

2. Ir a configuración del curso

   ![paso 1]({{fill prepath}}/assets/imgs/captura1.png)

3. Escoger la opción de Importar dentro del banco de preguntas.

   ![paso 2]({{fill prepath}}/assets/imgs/captura2.png)

4. Escoger importar en formato XML Moodle, arrastrar el fichero XML (hay que
   hacerlo por cada uno) y darle a importar.

   ![paso 3]({{fill prepath}}/assets/imgs/captura3.png)

<!-- "{{fill prepath}}/assets/imgs/julia" -->
