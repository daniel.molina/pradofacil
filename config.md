<!--
Add here global page variables to use throughout your
website.
The website_* must be defined for the RSS to work
-->
@def website_title = "Formulario para generar Moodle cuestionarios"
@def website_descr = "Formulario para generar Moodle cuestionarios"
@def website_url   = "https://pradofacil.danimolina.net"

@def author = "Daniel Molina"
@def div_content = "container"
@def prepath = ""
+++
using Sockets
server = "pradofacil.danimolina.net:8100"
+++

<!--
Add here global latex commands to use throughout your
pages. It can be math commands but does not need to be.
For instance:
* \newcommand{\phrase}{This is a long phrase to copy.}
-->
\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}


\newcommand{\title}[1]{
    ~~~
    <h1 class="title is-4">#1</h1>
    ~~~
}

\newcommand{\subtitle}[1]{
    ~~~
    <h2 class="subtitle is-4">#1</h2>
    ~~~
}

\newcommand{\subsection}[1]{
    ~~~
    <h2 class="subtitle is-5">#1</h2>
    ~~~
}

\newcommand{\subsubsection}[1]{
    ~~~
    <h2 class="subtitle is-6">#1</h2>
    ~~~
}

\newcommand{\section}{
    ~~~
    <div class="tabs is-medium">#1</h2>
    ~~~
}
