@def title = "Formulario para crear cuestionarios Moodle"
@def hasmath = false
@def hascode = false
<!-- Note: by default hasmath == true and hascode == false. You can change this in
the config file by setting hasmath = false for instance and just setting it to true
where appropriate -->

~~~
<div class="content is-medium">
~~~

\title{Formulario para generar Cuestionarios Moodle}

\subtitle{Motivación}

Con la llegada del coronavirus, el uso de PRADO/Moodle se ha convertido en una herramienta imprescindible, especialmente para la realización de pruebas evaluables o de autoevaluación mediante el uso de Cuestionarios.

Para facilitar la creación de este tipo de cuestionarios al profesorado en general, esta página web permite generar, a partir de una sintaxis muy sencilla, ficheros que pueden importarse en PRADO como Bancos de preguntas (como formato XMLMoodle). 

\textinput{menu.md}
~~~
</div>
~~~
