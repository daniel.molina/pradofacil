@def title = "Formulario para crear cuestionarios Moodle"
@def hasmath = false
@def hascode = false
<!-- Note: by default hasmath == true and hascode == false. You can change this in
the config file by setting hasmath = false for instance and just setting it to true
where appropriate -->
\textinput{motivacion}
\textinput{menu.md}

~~~
<article class="message is-info">
<div class="message-header">
Ejemplos
</div>
<div class="message-body">
* Categoría Salud<br><br>
<p>No es recomendable el uso de mascarillas. -</p>
<p>Las mascarillas quirúrgicas son desechables (de un solo uso). +</p>
<p>
El coronavirus es el virus, y la enfermedad se llama:<br>
- Sueño.<br>
+ Covid-19.<br>
- Resfriado.<br>
</p>
<p>A la hora de salir debo:<br>
+ Intentar mantener la distancia de seguridad.<br>
- Ir rápido y con ropa de deporte.<br>
+ Ir con mascarilla.<br>
</p>
<p>
Los animales que cazan otros animales para comer se llaman... [carnívoros]
</p>
<p>
A la hora de ir de compras debo:<br>
- Respetar la distancia en la cola.<br>
- Entrar sin mirar.<br>
- Cumplir el límite de aforo.<br>
+ La opción A y C son correctas.<br>
</p>
* Categoría Deporte<br><br>
<p>
Rafa Nadal es un deportista, ¿de qué deporte?<br>
- Natación.<br>
+ Tenis.<br>
- Fútbol.<br>
- Baloncesto.<br>
</p>
</div>
</article>
~~~
