#!/bin/sh
julia --project=. -e 'using MoodleQuestions; serve_quiz(; port=8100)' &
julia --project=. -e 'using Franklin; serve()'
